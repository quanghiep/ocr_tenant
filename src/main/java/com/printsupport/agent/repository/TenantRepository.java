package com.printsupport.agent.repository;

import com.printsupport.agent.modal.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TenantRepository extends JpaRepository<Tenant, String> {
    Tenant findByTenantName(String name);
}
