package com.printsupport.agent.multitenancy;

import com.printsupport.agent.modal.Tenant;
import com.printsupport.agent.repository.TenantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Alon Segal on 24/03/2017.
 */
@Component
public class TenantNameFetcher extends UnboundTenantTask<Tenant> {

    @Autowired
    private TenantRepository userRepository;

    @Override
    protected Tenant callInternal() {
        Tenant utr = userRepository.findByTenantName(this.username);
        return utr;
    }
}
