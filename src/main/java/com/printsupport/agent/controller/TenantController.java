package com.printsupport.agent.controller;

import com.printsupport.agent.constant.Constant;
import com.printsupport.agent.modal.Message;
import com.printsupport.agent.modal.Tenant;
import com.printsupport.agent.repository.MessageRepository;
import com.printsupport.agent.repository.TenantRepository;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
public class TenantController {

    @Autowired
    TenantRepository tenantRepository;

    @Autowired
    MessageRepository messageRepository;

    @GetMapping("/myname")
    public String myname() {
        List<Tenant> tenant = tenantRepository.findAll();
        return tenant.size() > 0 ? tenant.get(0).getTenantName() : "not-found";
    }

    @PostMapping(value = "/api/user/scan/single")
    public ResponseEntity<?> singleFileUpload(@RequestParam("file") MultipartFile file, @RequestParam(name = "lang", required = false) String lang) throws IOException, TesseractException {
        File convFile = convert(file);
        Tesseract tesseract = new Tesseract();
        if(lang != null && Constant.SUPPORT_LANG.contains(lang))
            tesseract.setLanguage(lang);
        tesseract.setDatapath("D://Project//prinsupport//tessdata");
        String text = tesseract.doOCR(convFile);
        messageRepository.save(new Message(text, new Date()));
        return ResponseEntity.ok(text);
    }

    public static File convert(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }
}
