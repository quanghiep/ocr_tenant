package com.printsupport.agent.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;

import javax.xml.bind.DatatypeConverter;
import java.io.Serializable;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.function.Function;

@Component
public class JwtTokenUtil implements Serializable {
    private static final long serialVersionUID = -2550185165626007488L;
    public static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;

    private String privateKey = "private-key";
    //retrieve username from jwt token
    public String getUsernameFromToken(String token) {
        try {
            token = token.replace("bearer ", "");
            token = token.replace("Bearer ", "");
            Claims claims =  getAllClaimsFromToken(token);
            return claims.get("user_name").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //for retrieveing any information from token we will need the secret key
    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(privateKey.getBytes(StandardCharsets.UTF_8)).parseClaimsJws(token).getBody();
    }
}
